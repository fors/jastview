package jastview;


import java.lang.annotation.Annotation;
import java.lang.reflect.*;
import java.util.*;

public class Utils {
	//	public static void main(String args[]) {
	//		 System.out.println(getChildren(testast.A.class));
	//	}

	public static List<Child> getChildren(Class<?> c) {
		try {
			ArrayList<Child> children = new ArrayList<Child>();
			for (Method m : c.getMethods()) {
				for (Annotation a: m.getAnnotations()) {
					String annotationName = a.annotationType().getCanonicalName();
					if (annotationName.endsWith("ASTNodeAnnotation.Child")
							|| annotationName.endsWith("ASTNodeAnnotation.ListChild")
							|| annotationName.endsWith("ASTNodeAnnotation.OptChild")) {
						String name = "";
						if (a.getClass().getMethod("name") != null) {
							name = (String) a.getClass().getMethod("name").invoke(a, new Object[]{});
						}
						boolean isList = annotationName.endsWith("ASTNodeAnnotation.ListChild");
						boolean isOpt = annotationName.endsWith("ASTNodeAnnotation.OptChild");
						children.add(new Child(m, name, isList, isOpt));
					}
				}
			}
			return children;
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return null;
	}

	public static List<Token> getTokens(Class<?> c) {
		try {
			ArrayList<Token> tokens = new ArrayList<Token>();
			for (Method m : c.getMethods()) {
				for (Annotation a: m.getAnnotations()) {
					if (a.annotationType().getCanonicalName().endsWith("ASTNodeAnnotation.Token")) {
						String name = "";
						if (a.getClass().getMethod("name") != null) {
							name = (String) a.getClass().getMethod("name").invoke(a, new Object[]{});
						}
						tokens.add(new Token(m, name));
					}
				}
			}
			return tokens;
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return null;
	}

	public static List<Method> getAttributes(Class<?> c) {
		try {
			ArrayList<Method> attributes = new ArrayList<Method>();
			for (Method m : c.getMethods()) {
				for (Annotation a: m.getAnnotations()) {
					if (a.annotationType().getCanonicalName().endsWith("ASTNodeAnnotation.Attribute")) {
						attributes.add(m);
					}
				}
			}
			return attributes;
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return null;
	}

	public static class Token {
		public final Method method;
		public final String name;
		public Token(Method method, String name) {
			this.method = method;
			this.name = name;
		}
		public String toString() {
			return name + " -> " + method.getName();
		}
	}

	public static class Child {
		public final Method method;
		public final String name;
		public final boolean isList;
		public final boolean isOpt;

		public Child(Method method, String name, boolean isList, boolean isOpt) {
			this.method = method;
			this.name = name;
			this.isList = isList;
			this.isOpt = isOpt;
		}

		public String toString() {
			return name + " -> " + method.getName() + "(isList: " + isList + ", isOpt: " + isOpt + ")";
		}
	}
}
