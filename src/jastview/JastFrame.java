package jastview;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeSelectionModel;

public class JastFrame extends JFrame {
	private static final long serialVersionUID = 1L;

	private JTree tree;
	private DefaultMutableTreeNode treeRoot;
	private final String[] columnNames = new String[]{"name", "value"};
	private JTable table;

	private final Object astRoot;

	public JastFrame(Object astRoot) {
		this.astRoot = astRoot;
		setLayout(new BorderLayout());

		createTreeNodes();
		tree = new JTree(treeRoot);
		tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		tree.addTreeSelectionListener(new TreeSelectionListener() {
			public void valueChanged(TreeSelectionEvent e) {
				updateTable();
			}
		});
		JScrollPane treeScrollPane = new JScrollPane(tree);
		treeScrollPane.setPreferredSize(new Dimension(400, 80));

		table = new JTable(new Object[][]{}, columnNames);
		table.setEnabled(false);
		JScrollPane tableScrollPane = new JScrollPane(table);

		JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, 
				treeScrollPane, tableScrollPane);

		add(splitPane, BorderLayout.CENTER);

		setTitle("JastView");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		pack();
		setVisible(true);
	}

	private void updateTable() {
		DefaultMutableTreeNode node = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
		if (node == null) return;

		Object nodeInfo = node.getUserObject();
		TreeObject h = (TreeObject)nodeInfo;

		List<Utils.Token> tokens = Utils.getTokens(h.object.getClass());
		List<Method> attributes = Utils.getAttributes(h.object.getClass());
		Collections.sort(attributes, new Comparator<Method>() {
			public int compare(Method m1, Method m2) {
				return m1.getName().compareTo(m2.getName());
			}
		});

		Object[][] data = new Object[tokens.size() + attributes.size() + 2][2];
		data[0][0] = "identityHashCode";
		data[0][1] = Integer.toHexString(System.identityHashCode(h.object));
		int i = 1; 
		for (Utils.Token token: tokens) {
			try {
				data[i][0] = token.name;
				data[i][1] = token.method.invoke(h.object, new Object[]{});
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			i++;
		}
		data[i][0] = "";
		data[i][1] = "";
		i++;

		for (Method m: attributes) {
			Object value;
			try {
				if (m.getParameterTypes().length == 0) {
					value = m.invoke(h.object, new Object[]{});
					if (value == null) value = "null";
				} else {
					value = "<paramerized attribute>";
				}
			} catch (Exception ex) {
				System.err.println("During call to attribute " + m.getName());
				ex.printStackTrace();
				value = "<exception>";
			}
			data[i][0] = m.getName();
			data[i][1] = value;
			i++;	            	
		}
		table.setModel(new DefaultTableModel(data, columnNames));
	}

	private void createTreeNodes() {
		treeRoot = new DefaultMutableTreeNode(new TreeObject(astRoot, ""));
		createTreeNodesRecursive(treeRoot, astRoot);
	}

	private void createTreeNodesRecursive(DefaultMutableTreeNode parentTreeNode, Object parent) {
		for (Utils.Child c: Utils.getChildren(parent.getClass())) {
			try {
				Object child = c.method.invoke(parent, new Object[]{});
				TreeObject h = new TreeObject(child, c.name);
				DefaultMutableTreeNode treeNode = new DefaultMutableTreeNode(h);
				parentTreeNode.add(treeNode);
				if (c.isList || c.isOpt) {
					for (Object listChild: (Iterable<?>) child) {
						TreeObject h2 = new TreeObject(listChild, "");
						DefaultMutableTreeNode treeNode2 = new DefaultMutableTreeNode(h2);
						treeNode.add(treeNode2);
						createTreeNodesRecursive(treeNode2, listChild);
					}
				} else {
					createTreeNodesRecursive(treeNode, child);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private static class TreeObject {
		public final Object object;
		public final String prefix;
		public TreeObject(Object object, String prefix) {
			this.object = object;
			this.prefix = prefix;
		}
		public String toString() {
			String c = object.getClass().getSimpleName();
			if (prefix.isEmpty() || prefix.equals(c)) {
				return c;
			} else {
				return prefix + ":" + c;
			}
		}
	}
}
